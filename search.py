# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util


class DestNotReachableFromSource(Exception):
    pass


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return [s, s, w, s, w, w, s, w]


def _findPathWithoutCostsGraphSearch(problem, vertexSuccessorsUtilType: callable):
    currentVertex = problem.getStartState()
    visitedSuccessors = {currentVertex: []}
    currentVertexSuccessors = vertexSuccessorsUtilType()
    for vertex, direction, value in problem.getSuccessors(currentVertex):
        currentVertexSuccessors.push((vertex, [direction]))
    while not currentVertexSuccessors.isEmpty():
        currentVertex, currentPath = currentVertexSuccessors.pop()
        if currentVertex in visitedSuccessors.keys():
            continue
        visitedSuccessors[currentVertex] = currentPath
        if problem.isGoalState(currentVertex):
            return visitedSuccessors[currentVertex]
        for vertex, direction, value in problem.getSuccessors(currentVertex):
            currentVertexSuccessors.push((vertex, currentPath + [direction]))
    raise DestNotReachableFromSource()


def depthFirstSearch(problem):
    """Search the deepest nodes in the search tree first."""
    return _findPathWithoutCostsGraphSearch(problem, util.Stack)


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    return _findPathWithoutCostsGraphSearch(problem, util.Queue)


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    PATH_COST_INDEX, PATH_INDEX = 0, 1
    currentVertex = problem.getStartState()
    visitedSuccessors = {currentVertex: (0, [])}
    currentVertexSuccessors = util.PriorityQueue()
    for vertex, direction, value in problem.getSuccessors(currentVertex):
        currentVertexSuccessors.push((vertex, [direction], value), value)
    while not currentVertexSuccessors.isEmpty():
        currentVertex, currentPath, currentValue = currentVertexSuccessors.pop()
        if currentVertex in visitedSuccessors.keys():
            if visitedSuccessors[currentVertex][PATH_COST_INDEX] > currentValue:
                visitedSuccessors[currentVertex] = (currentValue, currentPath)
            continue
        visitedSuccessors[currentVertex] = (currentValue, currentPath)
        if problem.isGoalState(currentVertex):
            return visitedSuccessors[currentVertex][PATH_INDEX]
        for vertex, direction, value in problem.getSuccessors(currentVertex):
            newValue = currentValue + value
            currentVertexSuccessors.push((vertex, currentPath + [direction], newValue), newValue)
    raise DestNotReachableFromSource()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0


def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    PATH_INDEX = 1
    currentVertex = problem.getStartState()
    vertexToF = {currentVertex: (0 + heuristic(currentVertex, problem), [])}
    currentVertexSuccessors = util.PriorityQueue()
    for vertex, direction, value in problem.getSuccessors(currentVertex):
        currentVertexSuccessors.push((vertex, [direction], value), value + heuristic(vertex, problem))
    while not currentVertexSuccessors.isEmpty():
        currentVertex, currentPath, currentValue = currentVertexSuccessors.pop()
        if currentVertex in vertexToF.keys():
            continue
        vertexToF[currentVertex] = (currentValue, currentPath)
        if problem.isGoalState(currentVertex):
            return vertexToF[currentVertex][PATH_INDEX]
        for vertex, direction, value in problem.getSuccessors(currentVertex):
            gValue = value + currentValue
            currentVertexSuccessors.push((vertex, currentPath + [direction], gValue), gValue + heuristic(vertex, problem))
    raise DestNotReachableFromSource()



# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
